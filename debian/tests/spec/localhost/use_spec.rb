require 'spec_helper'

describe command('puppet apply --detailed-exitcodes --execute "include sssd"') do
  its(:exit_status) { should eq 2 }
end

describe package('sssd') do
  it { should be_installed }
end

describe file('/etc/sssd/sssd.conf') do
  it { should be_file }
  its(:content) { should match(/maintained by Puppet/) }
end

describe service('sssd') do
  it { should be_enabled }
  it { should be_running }
end
